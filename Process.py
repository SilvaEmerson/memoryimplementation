class Process:
    def __init__(self, *args):
        self._name = args[1]
        self._arrive_time = args[0]["arrive_time"]
        self._processing_time = args[0]["processing_time"]
        self._requested_memory = args[0]["requested_memory"]
        self._memory_location = None

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def arrive_time(self):
        return self._arrive_time

    @property
    def processing_time(self):
        return self._processing_time

    @property
    def requested_time(self):
        return self._requested_memory

    @property
    def memory_location(self):
        return self._memory_location

    @memory_location.setter
    def memory_location(self, location):
        self._memory_location = location
