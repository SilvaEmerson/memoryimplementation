from Process import *
AVAIBLE_MEMORY = 35
OS_MEMORY = 10
USER_PROCESS_MEMORY = 25

processes = {
    "p1": {"arrive_time": 0, "processing_time": 5, "requested_memory": 3},
    "p2": {"arrive_time": 3, "processing_time": 3, "requested_memory": 5},
    "p3": {"arrive_time": 7, "processing_time": 9, "requested_memory": 8},
    "p4": {"arrive_time": 12, "processing_time": 10, "requested_memory": 12},
    "p5": {"arrive_time": 18, "processing_time": 16, "requested_memory": 2},
    "p6": {"arrive_time": 25, "processing_time": 2, "requested_memory": 6},
    "p7": {"arrive_time": 29, "processing_time": 8, "requested_memory": 9},
}

memory = [*["-"] * (AVAIBLE_MEMORY - OS_MEMORY), *["OS"] * OS_MEMORY]

process_list = [*map(lambda el: Process(processes[el], el), processes)]

process_list = [*sorted(process_list, key=lambda el: el.arrive_time)]

total_processing_time = sum([*map(lambda el: el.requested_time, process_list)])
