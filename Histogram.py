#!/usr/bin/env python
# coding: utf-8

import matplotlib.pyplot as plt
from os.path import getsize
from statistics import median
import sys import argv
import os

def get_all_files(*dirs):
    files_size = []
    for dir in dirs:
        for root, dirs, files in os.walk(dir):
            for name in files:
                try:
                    files_size.append(getsize(os.path.join(root, name)))
                except:
                    pass
    return files_size


if __name__ == '__main__':
    files_size = get_all_files('/usr/bin', '/bin', '/local/bin')

    plt.hist(files_size, bins=90)
    plt.xlabel('File size (Bytes)')
    plt.ylabel('Absolute Frequency')
    plt.show()

    print("Média: %2.f bytes" % (sum(files_size) / len(files_size)))
    print("Mediana:", median(files_size), "bytes")
        
