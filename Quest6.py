from sys import argv
import os
from os.path import getsize

def ls(*dirs):
    for directory in dirs:
        for root, dirs, files in os.walk(directory):
            for file_name in files:
                print('%s |--- %s' % (root, file_name))


if __name__ == '__main__':
    print('DIRECTORY | FILE')
    ls(*argv)
