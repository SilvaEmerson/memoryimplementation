from utils import processes, AVAIBLE_MEMORY, OS_MEMORY, USER_PROCESS_MEMORY, memory, process_list, total_processing_time
from Process import Process


print("Original memory state: |%s|" % ("|".join(memory)))


def first_fit(current_memory, processes, total_processing_time):
    new_memory = current_memory.copy()[::-1]

    def alocate(process):
        value_to_store = process.requested_time
        for ind, el in enumerate(new_memory):
            if new_memory[ind : ind + value_to_store].count("-") == value_to_store:
                new_memory[ind : ind + value_to_store] = [process.name] * value_to_store
                return (ind, ind + value_to_store)

    def desalocate(begin_adress, end_adress):
        new_memory[begin_adress:end_adress] = ["-"] * (end_adress - begin_adress)

    for time in range(total_processing_time + 1):
        for process in processes:
            if time == process.arrive_time:
                process.memory_location = alocate(process)
            if time == (process.arrive_time + process.processing_time):
                desalocate(*process.memory_location)

        print("\nTime: %d\nMemory: |%s|" % (time, "|".join(new_memory[::-1])))


if __name__ == "__main__":
    first_fit(memory, process_list, total_processing_time)
