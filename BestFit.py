from utils import total_processing_time, memory, process_list


def best_fit(memory, processes, total_processing_time):
    new_memory = memory.copy()

    def map_memory_holes(memory):
        map_holes = []
        counter = 0
        for ind, slot in enumerate(memory):
            if slot == "-":
                counter += 1
            elif counter > 0:
                lowest_ind = ind - counter
                largest_ind = lowest_ind + counter
                map_holes.append((lowest_ind, largest_ind))
                counter = 0

        return map_holes

    def alocate(memory, process, memory_holes):
        value_to_store = process.requested_time
        min_adress, max_adress = min(
            [i for i in memory_holes if i[1] - i[0] >= value_to_store]
        )
        max_adress = min_adress + value_to_store
        memory[min_adress:max_adress] = [process.name] * value_to_store
        return memory, (min_adress, max_adress)

    def desalocate(begin_adress, end_adress):
        new_memory[begin_adress:end_adress] = ["-"] * (end_adress - begin_adress)

    for time in range(total_processing_time + 1):
        for process in processes:
            if time == process.arrive_time:
                new_memory, process.memory_location = alocate(
                    new_memory, process, map_memory_holes(new_memory)
                )
            if time == (process.arrive_time + process.processing_time):
                desalocate(*process.memory_location)

        print("\nTime: %d\nMemory: |%s|" % (time, "|".join(new_memory)))


best_fit(memory, process_list, total_processing_time)
